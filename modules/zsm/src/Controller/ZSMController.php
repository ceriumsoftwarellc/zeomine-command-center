<?php
/**
 * @file
 */

namespace Drupal\zsm\Controller;

use Drupal\Core\Controller\ControllerBase;
//use Drupal\Core\Cache\CacheableJsonResponse
use Drupal\Core\Cache\CacheableMetadata;
use \Drupal\zsm\Entity\ZSMCore;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ZSMController
 *
 * Provides the route controller for zsm.
 */
class ZSMController extends ControllerBase
{
    /**
     * Finds a task and sends it to the crawler
     *
     * @return response;
     *   The settings data to return
     */
    public function get_settings_json($uuid) {
        $resp = $this->get_settings_data($uuid);
        if ($resp) {
            // Add the zsm_core cache tag so the endpoint results will update when nodes are
            // updated.
            $cache_metadata = new CacheableMetadata();
            $cache_metadata->setCacheTags(['zsm_core']);

            // Create the JSON response object and add the cache metadata.
            //$response = new CacheableJsonResponse($response_array);
            //$response->addCacheableDependency($cache_metadata);
            $response = new JsonResponse($resp);

            return $response;
        }
        else {
            $response = new JsonResponse([]);
            return $response;
        }
    }

    /**
     * Finds a task and sends it to the crawler
     *
     * @return response;
     *   The settings data to return
     */
    public function get_settings_data($uuid)
    {
        // Check the uuid
        $check = FALSE;
        if (is_string($uuid) && (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) === 1)) {
            $check = TRUE;
        }
        if ($check) {
            $n = \Drupal::entityTypeManager()->getStorage('zsm_core')->loadByProperties(['uuid' => $uuid]);
            $id = array_keys($n)[0];
            if ($id) {
                $core = ZSMCore::load($id);

                // Get the core settings
                $data = ['settings' => [], 'conf' => []];
                $settings_map = \Drupal::service('entity_field.manager')->getFieldDefinitions('zsm_core', 'zsm_core');
                // Clean out DB items that do not go into the settings
                $settings_map = array_keys($settings_map);
                $exclude = ['id', 'uuid', 'title', 'user_id', 'created', 'changed'];
                $settings_map = array_diff($settings_map, $exclude);
                foreach ($settings_map as $key) {
                    if (strpos($key, 'path') !== FALSE) {
                        if ($val = $core->get($key)->getValue()) {
                            if (isset($val[0]['value'])) {
                                $val = $val[0]['value'];
                                $k = str_replace('path_', '', $key);
                                $data['settings']['user_data'][$k] = $val;
                            }
                        }

                    }
                    else if (strpos($key, 'field_zsm_enabled_plugins') !== FALSE) {
                        if ($vals = $core->get($key)->getValue()) {
                            foreach($vals as $key => $val) {
                                $plug = \Drupal::entityTypeManager()->getStorage($val['target_type'])->load($val['target_id']);
                                $plug_data = $plug->getZSMPluginData();
                                $data['conf']['plugins'][$plug_data['class']] = $plug->getZSMPluginSettings();
                            }
                        }
                    }
                    else {
                        if ($val = $core->get($key)->getValue()) {
                            if (isset($val[0]['value'])) {
                                $val = $val[0]['value'];
                                $data['settings'][$key] = $val;
                            }
                        }
                    }
                }
                return $data;
            }
            else {
                return FALSE;
            }
        }
        else {
            return FALSE;
        }
    }
}