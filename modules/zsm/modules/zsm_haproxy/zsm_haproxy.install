<?php
/**
 * @file
 */

use Drupal\Core\Config\ConfigFactoryOverrideInterface;

/**
 * Implements hook_install().
 *
 * This will install/reinstall all of the defaults for this module - if you are using custom settings,
 * please CLONE views and groups and use these as a template.
 */
function zsm_haproxy_install() {
    \Drupal::service('config.installer')->installDefaultConfig('module', 'zsm_haproxy');
    /**
     * Install the custom configs for the ZSM Core object
     */
    $storage_config = \Drupal::configFactory()->getEditable('field.storage.zsm_core.field_zsm_enabled_plugins');
    $settings = $storage_config->getOriginal('settings');
    $settings['entity_type_ids']['zsm_haproxy_plugin'] = 'zsm_haproxy_plugin';
    $storage_config->set('settings', $settings)->save();

    $field_config = \Drupal::configFactory()->getEditable('field.field.zsm_core.zsm_core.field_zsm_enabled_plugins');
    $settings = $field_config->getOriginal('settings');
    $settings['zsm_haproxy_plugin'] = array(
        'handler' => 'default:zsm_haproxy_plugin',
        'handler_settings' => array(
            'target_bundles' => NULL,
            'sort' => array(
                'field' => '_none',
            ),
            'auto_create' => FALSE,
        ),
    );
    $field_config->set('settings', $settings)->save();

    drupal_set_message(t('ZCC: Server Monitor - HAProxy Plugin has been installed.'), 'status', TRUE);
}

/**
 * Implements hook_uninstall().
 */
function zsm_haproxy_uninstall() {
    \Drupal::service('config.manager')->uninstall('module', 'zsm_haproxy');

    /**
     * Uninstall custom configs for ZSM Core object
     */
    $storage_config = \Drupal::configFactory()->getEditable('field.storage.zsm_core.field_zsm_enabled_plugins');
    $settings = $storage_config->getOriginal('settings');
    unset($settings['entity_type_ids']['zsm_haproxy_plugin']);
    $storage_config->set('settings', $settings)->save();

    $field_config = \Drupal::configFactory()->getEditable('field.field.zsm_core.zsm_core.field_zsm_enabled_plugins');
    $settings = $field_config->getOriginal('settings');
    unset($settings['zsm_haproxy_plugin']);
    $field_config->set('settings', $settings)->save();

}
