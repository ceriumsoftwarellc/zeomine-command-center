<?php

/**
 * @file
 */

namespace Drupal\zsm_backup_date\Entity\Views;

use Drupal\views\EntityViewsData;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides a view to override views data for test entity types.
 */
class ZSMBackupDatePluginViewsData extends EntityViewsData {

    /**
     * {@inheritdoc}
     */
    public function getViewsData() {
        $data = parent::getViewsData();

        return $data;
    }

}
?>