<?php

/**
 * @file
 */


/**
 * Implements hook_views_data().
 */

//function zsm_haproxy_views_data() {
//    $data['zsm_haproxy_plugin'] = [];
//    $data['zsm_haproxy_plugin']['table'] = [];
//    $data['zsm_haproxy_plugin']['table']['group'] = 'ZSM HAProxy Plugin Settings';
//    $data['zsm_haproxy_plugin']['table']['provider'] = 'zsm_haproxy_plugin';
//    $data['zsm_haproxy_plugin']['table']['base'] = array(
//        'field' => 'id',
//        'title' => t('ZSM HAProxy Plugin Settings'),
//        'help' => t('ZSM HAProxy Plugin Settings Items'),
//        'weight' => -10,
//    );
//
//    /**
//     * Basics
//     */
//    $data['zsm_haproxy_plugin']['id'] = [
//        'title' => t('ID'),
//        'help' => t('Numeric ID of the ZSM HAProxy Plugin Settings'),
//        'field' => ['id' => 'numeric'],
//        'sort' => ['id' => 'standard'],
//        'filter' => ['id' => 'numeric'],
//        'argument' => ['id' => 'numeric'],
//    ];
//    $data['zsm_haproxy_plugin']['uuid'] = [
//        'title' => t('UUID'),
//        'help' => t('UUID of the ZSM HAProxy Plugin Settings'),
//        'field' => ['id' => 'standard'],
//        'sort' => ['id' => 'standard'],
//        'filter' => ['id' => 'string'],
//        'argument' => ['id' => 'string'],
//    ];
//    $data['zsm_haproxy_plugin']['created'] = [
//        'title' => t('Post Date'),
//        'help' => t('Moment the ZSM HAProxy Plugin Settings were created'),
//        'field' => ['id' => 'date'],
//        'sort' => ['id' => 'date'],
//        'filter' => ['id' => 'date'],
//    ];
//    $data['zsm_haproxy_plugin']['changed'] = [
//        'title' => t('Updated Date'),
//        'help' => t('Moment the ZSM HAProxy Plugin Settings were last updated'),
//        'field' => ['id' => 'date'],
//        'sort' => ['id' => 'date'],
//        'filter' => ['id' => 'date'],
//    ];
//    $data['zsm_haproxy_plugin']['title'] = [
//        'title' => t('Title'),
//        'help' => t('Title of the ZSM HAProxy Plugin Instance'),
//        'field' => ['id' => 'standard'],
//        'sort' => ['id' => 'standard'],
//        'filter' => ['id' => 'string'],
//        'argument' => ['id' => 'string'],
//    ];
//    $data['zsm_haproxy_plugin']['user_id'] = [
//        'title' => t('Author UID'),
//        'help' => t('Numeric ID of the ZSM HAProxy Plugin Instance'),
//        'field' => ['id' => 'numeric'],
//        'sort' => ['id' => 'standard'],
//        'filter' => ['id' => 'numeric'],
//        'argument' => ['id' => 'numeric'],
//    ];
//    /**
//     * Metadata
//     */
//    $data['zsm_haproxy_plugin']['description'] = [
//        'title' => t('Description'),
//        'help' => t('Description of the ZSM HAProxy Plugin Instance. Used in the report metadata.'),
//        'field' => ['id' => 'standard'],
//        'sort' => ['id' => 'standard'],
//        'filter' => ['id' => 'string'],
//        'argument' => ['id' => 'string'],
//    ];
//
//    /**
//     * Verbosity/Logging
//     */
//
//
//
//
//    return $data;
//}
//
