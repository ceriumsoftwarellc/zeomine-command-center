<?php
/**
 * @file
 */

namespace Drupal\zcc_zeomine\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides the route controller for kb.
 */

class ZCCZeomineController extends ControllerBase
{

    /**
     * Finds a task and sends it to the crawler
     *
     * @return $data;
     *   The task settings data to return
     */
    public function get_task()
    {
        $data = json_encode(array());

        return $data;
    }
}